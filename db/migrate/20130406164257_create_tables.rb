class CreateTables < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name, :email, :password_hash
    end
    create_table :blogs do |t|
      t.string :name
      t.references :user
    end

    create_table :posts do |t|
      t.string :title 
      t.text :body
      t.timestamps
      t.references :blog
    end
  
    create_table :tags, :unique => true do |t|
      t.string :name
    end
 
    create_table :posts_tags do |t|
      t.references :post, :tag
    end
    add_index :posts_tags, [:post_id, :tag_id]
  end
end






