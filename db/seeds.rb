require 'Faker'

10.times do 

  user = User.create :name => Faker::Name.first_name,
              :email => Faker::Internet.email,
              :password_hash => "password"
              # :password_confirmation => "password"
  blog = Blog.create :name => Faker::Company.bs

  blog.user = user 
  blog.save

  post1 = Post.create :title => Faker::Company.bs,
                     :body  => Faker::Lorem.paragraphs(4)
  post2 = Post.create :title => Faker::Company.bs,
                     :body  => Faker::Lorem.paragraphs(5)
  post3 = Post.create :title => Faker::Company.bs,
                     :body  => Faker::Lorem.paragraphs(2)
 
  blog_id = Blog.last.id

  post1.blog_id = blog_id
  post1.save
  post2.blog_id = blog_id
  post2.save
  post3.blog_id = blog_id
  post3.save




end



tag1 = tag2 = tag3 = nil

9.times do |i|
  t = Tag.create :name => Faker::Lorem.word
  tag1 = t if i % 3 == 0
  tag2 = t if i % 3 == 1
  tag3 = t if i % 3 == 2
end

Post.all.each do |post|
  post.tags << tag1
  post.tags << tag2
  post.tags << tag3
  post.save
end

